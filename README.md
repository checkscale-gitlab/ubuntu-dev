# ubuntu-dev

A Docker image based on Ubuntu including development libraries & tools.

Development tools:
 - build-essential (gcc, g++, make)
 - clang
 - cmake
 - kcov (from https://github.com/SimonKagstrom/kcov)
 - llvm
 - make
 - pkg-config
 - python-minimal

Development libraries:
 - binutils-dev
 - libclang-dev
 - libdw-dev
 - libiberty-dev
 - libjemalloc-dev
 - libpcap-dev
 - libssl-dev
 - zlib1g-dev

Misc. tools:
 - debsigs
 - unzip
 - zip

## Setup

* Install docker: https://docs.docker.com/engine/installation/
* Build the container image: `make build`
* Try the container: `make shell`
